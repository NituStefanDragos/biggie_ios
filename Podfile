# Podfile
source 'https://github.com/CocoaPods/Specs.git'
use_frameworks!
platform :ios, '9.0'

target 'Biggie_iOS' do
  # networking library
  pod 'Alamofire', '~> 4.0'
  # an easy way to work with JSON data
  pod 'SwiftyJSON'
  # easy way to use Alamofire and SwiftyJSON together
  pod 'AlamofireSwiftyJSON'
  # reactive programming; managing events
  # RxSwift is a Swift implementation of Reactive Extensions
  pod 'RxSwift',    '3.0.0-beta.2'
  # RxSwift Cocoa extensions
  pod 'RxCocoa',    '3.0.0-beta.2'
  # Facebook SDK
  pod 'FacebookCore'
  pod 'FacebookLogin'
  pod 'FacebookShare'
  # Codeless drop-in universal library allows to prevent issues of keyboard sliding up and cover UITextField/UITextView.
  pod 'IQKeyboardManagerSwift'
  # Material is an animation and graphics framework that is used to create beautiful applications.
  pod 'Material'
  # A drop-in universal solution for moving text fields out of the way of the keyboard in iOS.
  pod 'TPKeyboardAvoiding'
end

post_install do |installer|
  installer.pods_project.targets.each do |target|
    target.build_configurations.each do |config|
      config.build_settings['SWIFT_VERSION'] = '3.0'
    end
  end
end