//
//  RegisterView.swift
//  Biggie_iOS
//
//  Created by Stefan on 10/20/16.
//  Copyright © 2016 Ingenious IT Ideas. All rights reserved.
//

import Material
import Foundation
import RxCocoa
import RxSwift

class RegisterView: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var firstName: CustomTextField!
    @IBOutlet weak var lastName: CustomTextField!
    @IBOutlet weak var email: CustomTextField!
    @IBOutlet weak var password: CustomTextField!
    @IBOutlet weak var confirmPassword: CustomTextField!
    @IBOutlet var fields: [CustomTextField]!
    @IBOutlet weak var register: UIButton!
    let disposeBag = DisposeBag()
    
    var x : ControlEvent<Void>!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupTextField(textField: self.email,
                placeHolder: "Email",
                errorMessage: "Invalid email address.",
                emptyMessage: "email address has not been filled",
                rule: Constants.emailRegex)
        setupTextField(textField: self.firstName,
                placeHolder: "First Name",
                errorMessage: "Invalid First Name.",
                emptyMessage: "First Name has not been filled",
                rule: Constants.nameRegex)
              setupTextField(textField: self.lastName,
                      placeHolder: "Last Name",
                      errorMessage: "Invalid Last Name.",
                      emptyMessage: "Last Name has not been filled",
                      rule: Constants.nameRegex)
        setupTextField(textField: self.password,
                placeHolder: "Password",
                errorMessage: "Invalid Last Name.",
                emptyMessage: "Password has not been filled",
                rule: Constantns.passwordRegex)
        setupTextField(textField: self.confirmPassword,
                placeHolder: "Confirm Password",
                errorMessage: "Invalid Confirm Password.",
                emptyMessage: "Confirm Password has not been filled",
                rule: Constantns.noRegex)
        x = register.rx.controlEvent([.touchUpInside])
        x.subscribe(onNext: { (_: ()) in
            self.verifyFields()
        }, onError: { (error) in
            print(error)
        }, onCompleted: {
            print("completed")
        }, onDisposed: {
            print("disposed")
        }).addDisposableTo(disposeBag)
        
        //        setEmailVerification(textObservable:email.rx.textInput.text).bindTo(email.detailLabel.rx.hidden).addDisposableTo(disposeBag)
    }
    
    func setupTextField(textField: CustomTextField, placeHolder: String, errorMessage: String, emptyMessage: String, rule: String) {
        textField.placeholder = placeHolder
        textField.detailLabel.text = ""
        textField.detailLabel.isHidden = false
        textField.isClearIconButtonEnabled = true
        textField.emptyMessage = emptyMessage
        textField.errorMessage = errorMessage
        textField.rule = rule
//        textField.serverMessage = serverMessage
    }
    
    func assignPropperDetail(textField: ErrorTextField, errorMessage: String, emptyMessage: String) {
//        textField.rx.textInput.text.map { (text) -> String in
//            return text.isEmpty() ? emptyMessage : ""
//            }.skipUntil(register.rx.controlEvent([.touchUpInside])).bindTo(textField.detailLabel.rx.text)
//            .addDisposableTo(disposeBag)s
//        textField.rx.textInput.text.skipUntil(x!).map { (text) -> String in
//            let x = text.isEmpty() ? emptyMessage : errorMessage
//            print(x)
//            return x
//            }.bindTo(email.detailLabel.rx.text)
//            .addDisposableTo(disposeBag)
    }
    
    func emptyFieldMapping(textObservable:ControlProperty<String>) -> Observable<Bool> {
        return textObservable.map { text -> Bool in
            return !text.isEmpty()
        }
    }

    func verifyFields() {
        for textField:ErrorTextField in fields {
//            print(textField.placeholder.)
//            emptyFieldMapping(textObservable: textField.rx.textInput.text).bindTo(textField.detailLabel.rx.hidden).addDisposableTo(disposeBag)
        }
    }
    
    func verifyRegex(field:CustomTextField) -> Bool {
        let pattern = try? NSRegularExpression(pattern: field.rule, options: .caseInsensitive)
        return pattern?.firstMatch(in: field.text!, options: [], range: NSMakeRange(0, field.text!.characters.count)) != nil
    }
    
    func verifyEmailForDuplicate() {
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
        
}

class CustomTextField : ErrorTextField {
    
    var errorMessage:String!
    var emptyMessage:String!
    var serverMessage:String!
    var rule:String!
    
}
