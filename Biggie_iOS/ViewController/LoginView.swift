//
//  LoginView.swift
//  Biggie_iOS
//
//  Created by Stefan on 10/15/16.
//  Copyright © 2016 Ingenious IT Ideas. All rights reserved.
//

import Foundation
import UIKit
import FacebookLogin
import FBSDKLoginKit

class LoginView: UIViewController, FBSDKLoginButtonDelegate{
    
    @IBOutlet weak var facebookLogin: FBSDKLoginButton!
    
    func loginButton(_ loginButton: FBSDKLoginButton!, didCompleteWith result: FBSDKLoginManagerLoginResult!, error: Error!) {
        if (error == nil) {
            print(FBSDKLoginManagerLoginResult.description())
        } else {
            print(error.localizedDescription)
        }
    }
    
    func loginButtonDidLogOut(_ loginButton: FBSDKLoginButton!) {
        print("User logged out...")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        facebookLogin.delegate = self
        facebookLogin.center = view.center
        facebookLogin.readPermissions = ["public_profile", "email"]
    }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

 


}
