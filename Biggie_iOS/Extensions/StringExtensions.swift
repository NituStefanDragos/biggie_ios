//
// Created by Stefan on 10/23/16.
// Copyright (c) 2016 Ingenious IT Ideas. All rights reserved.
//

import Foundation

extension String {

    func isEmpty() -> Bool{
        return self.characters.count == 0
    }
}
