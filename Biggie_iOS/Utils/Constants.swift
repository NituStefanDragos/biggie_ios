//
// Created by Stefan on 10/23/16.
// Copyright (c) 2016 Ingenious IT Ideas. All rights reserved.
//

import Foundation

struct Constants {
    static let firstNameRegex = "^([A-Z][a-z]+){1,10}$"
    static let lastNameRegex = "^([A-Z][a-z']+){2,10}$"
    static let emailRegex = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,6}"
    static let passwordRegex = "^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d!$%@#£€*?&]{5,}$"
    static let noRegex = "^.{5,}$"
    
    func validateEmail(candidate: String) -> Bool {
        let emailRegex = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,6}"
        return NSPredicate(format: "SELF MATCHES %@", emailRegex).evaluate(with: candidate)
    }
}
