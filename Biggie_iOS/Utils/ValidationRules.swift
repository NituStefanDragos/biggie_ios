//
//  ValidationRules.swift
//  Biggie_iOS
//
//  Created by Stefan on 10/24/16.
//  Copyright © 2016 Ingenious IT Ideas. All rights reserved.
//

import Foundation
import RxSwift

class ValidationRules {
    
    static func isValidName(name: String, minimumLength: Int, maximumLength: Int) -> Observable<Bool> {
        let capitalLetter = self.beginsWithCapitalLetter(candidate: name)
        let containsNumbers = self.containsNumbers(candidate: name)
        let specialCharacters = self.containsSpecialCharacters(candidate: name)
        let numberOfCharacters = self.hasValidNumberOfCharacters(name: name, minimumLength: minimumLength, maximumLength: maximumLength)
        return Observable.combineLatest(capitalLetter, containsNumbers, specialCharacters, numberOfCharacters){
            capitalLetter, containsNumbers, specialCharacters, numberOfCharacters in
            capitalLetter && !containsNumbers && !specialCharacters && numberOfCharacters
        }.distinctUntilChanged()
    }
  
    static func isValidEmail(candidate: String) -> Observable<Bool> {
        let emailRegex = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,8}"
        let predicate = NSPredicate(format: "SELF MATCHES %@", emailRegex)
        return Observable.just(predicate.evaluate(with: candidate))
    }
    
    static func isValidPassword(candidate: String, minimumLength: Int, maximumLength: Int) -> Observable<Bool> {
        let numberOfCharacters = self.hasValidNumberOfCharacters(name: candidate, minimumLength: minimumLength, maximumLength: maximumLength)
        let containsNumbers = self.containsNumbers(candidate: candidate)
        let capitalLetter = self.containsOneCapitalLetter(candidate: candidate)
        let specialCharacters = self.containsSpecialCharacters(candidate: candidate)
        return Observable.combineLatest(capitalLetter, containsNumbers, specialCharacters, numberOfCharacters){
            capitalLetter, containsNumbers, specialCharacters, numberOfCharacters in
            capitalLetter && containsNumbers && specialCharacters && numberOfCharacters
            }.distinctUntilChanged()
    }
    
    static func isMatchingPassword(password:String, confirmation:String) -> Observable<Bool>{
        return Observable.just(password == confirmation)
    }
    
    static func hasValidNumberOfCharacters(name:String, minimumLength:Int, maximumLength:Int) -> Observable<Bool> {
        let hasValidNumberOfCharacters = name.characters.count <= maximumLength && name.characters.count >= minimumLength
        return Observable.just(hasValidNumberOfCharacters)
    }
    
    static func beginsWithCapitalLetter(candidate:String) -> Observable<Bool> {
        let capitalLetterRegEx  = ".*[A-Z]+.*"
        let predicate = NSPredicate(format:"SELF MATCHES %@", capitalLetterRegEx)
        return Observable.just(predicate.evaluate(with: candidate.characters.first))
    }
    
    static func containsNumbers(candidate:String) -> Observable<Bool>  {
        let numberRegEx  = ".*[0-9]+.*"
        let predicate = NSPredicate(format:"SELF MATCHES %@", numberRegEx)
        return Observable.just(!predicate.evaluate(with: candidate))
    }
    
    
    static func containsOneCapitalLetter(candidate:String) -> Observable<Bool>  {
        let capitalLetterRegEx  = ".*[A-Z]+.*"
        let predicate = NSPredicate(format:"SELF MATCHES %@", capitalLetterRegEx)
        return Observable.just(!predicate.evaluate(with: candidate))
    }

    static func containsSpecialCharacters(candidate: String) -> Observable<Bool> {
        let specialCharacterRegEx  = ".*[!&^%$#@()/]+.*"
        let predicate = NSPredicate(format:"SELF MATCHES %@", specialCharacterRegEx)
        return Observable.just(predicate.evaluate(with: candidate))
    }
    
    
 
    
    
    
}
