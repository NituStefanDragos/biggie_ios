//
// Created by Stefan on 10/15/16.
// Copyright (c) 2016 Ingenious IT Ideas. All rights reserved.
//

import Foundation
import RxSwift
import Alamofire
import RxSwift

class BackendServicesUtils {


    static let sharedManager = BackendServicesUtils.getManager()

    static func httpPostForEndpoint(_ endPoint: String, params: [String: AnyObject]) -> Observable<( [String: AnyObject])> {
        let urlEndPoint = URL(string: endPoint)
        return Observable.create { observer in
            let task = sharedManager.request(urlEndPoint!, method: .post, parameters: params, encoding: URLEncoding.default, headers: [:]).responseJSON(completionHandler: { response in switch response.result {
            case .success(let JSON):
                let response = JSON as! [String: AnyObject]
                print(response)
                observer.on(.next(response))
                observer.on(.completed)
            case .failure(let error):
                observer.on(.error(error))
                }
            
            })
            return Disposables.create {
                task.cancel()
            }
        }
    }

    fileprivate static func getManager() -> Alamofire.SessionManager {
        let configuration = URLSessionConfiguration.default
        configuration.timeoutIntervalForRequest = TimeInterval(ConfigServices.TIMEOUTINTERVAL)
        configuration.timeoutIntervalForResource = TimeInterval(ConfigServices.TIMEOUTINTERVAL)
        configuration.requestCachePolicy = NSURLRequest.CachePolicy.useProtocolCachePolicy
        return Alamofire.SessionManager(configuration: configuration)
    }

}
